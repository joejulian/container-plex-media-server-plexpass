#!/bin/sh

PKGDIR=$(realpath pkg)
TAG=${TAG:=latest}
NOCACHE=${NOCACHE}

make_container() {
	docker build ${NOCACHE} . -t plex-media-server-plexpath:${TAG}
}

make_container
